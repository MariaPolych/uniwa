Odoo is an ERP system and a cloud-based website builder that lets you create sites and manage online operations.
The significant advantage of the software is its intuitive interface.

To set up a docker instance of Odoo using docker-compose download this .yml file and run 

> docker-compose up -d

An instance is already running at :

http://snf-871607.vm.okeanos.grnet.gr:8069 